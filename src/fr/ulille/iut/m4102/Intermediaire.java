package fr.ulille.iut.m4102;

/**
 * Cette classe introduit un intermédiaire entre la classe utilisatrice
 * et l'implémentation du traitement des chaînes.
 * Au début cette classe se contente de logger ce qui se passe puis
 * elle va évoluer pour accéder au service à distance.
 * Le comportement de cette classe est totalement transparent pour la
 * classe Utilisatrice qui au final utilise les mêmes méthodes que si elle
 * appelait directement la classe AlaChaine.
 */
public class Intermediaire implements AlaChaineInterface {
    private Client client;
    
    public Intermediaire() {
	// pour l'instant on accède directement au service après instanciation
    this.client = new Client("localhost", 2020);
    }
    
    public int nombreMots(String chaine) {
    	//System.out.println("Méthode : nombreMots");
    	//System.out.println("Paramètre : String chaine = " + chaine);
    	String[] tab = intermediaire("CALL:nombreMots:param[string,\""+chaine+"\"]").split(":");
    	//System.out.println("Type résultat : int / Valeur = " + tab[2]);
    	return Integer.parseInt(tab[2]);
    }

    public String asphyxie(String chaine) {
    	//System.out.println("Méthode : asphyxie");
    	//System.out.println("Paramètre : String chaine = " + chaine);
    	String tab[] = intermediaire("CALL:asphyxie:param[string,\""+chaine+"\"]").split(":");
		//System.out.println("Type résultat : String / Valeur = \"" + tab[2]);
		return tab[2];
    }

    public String leetSpeak(String chaine) {
    	//System.out.println("Méthode : leetSpeak");
    	//System.out.println("Paramètre : String chaine = " + chaine);
    	String tab[] = intermediaire("CALL:leetSpeak:param[string,\""+chaine+"\"]").split(":");
    	//System.out.println("Type résultat : String / Valeur = \"" + tab[2]);
    	return tab[2];
    }

    public int compteChar(String chaine, char c) {
    	//System.out.println("Méthode : compteChar");
    	//System.out.println("Paramètres : String chaine = " + chaine + " / char c = " + c);
    	String tab[] = intermediaire("CALL:compteChar:param[string,\""+chaine+"\"]:param[char,\""+c+"\"]").split(":");
    	//System.out.println("Type résultat : int / Valeur = " + tab[2]);
    	return Integer.parseInt(tab[2]);
    }
    
    public String intermediaire(String call) {
    	return client.envoyer(call);
    }

}
