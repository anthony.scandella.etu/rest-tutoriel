package fr.ulille.iut.m4102;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Cette classe se trouvera côté service
 * Elle recevra une requête sous forme de chaîne
 * de caractère conforme à votre protocole.
 * Elle transformera cette requête en une
 * invocation de méthode sur le service puis
 * renverra le résultat sous forme de chaîne
 * de caractères.
 */
public class AccesService {
    private AlaChaine alc;
    private Socket client;
    
    public AccesService(Socket client) {
	alc = new AlaChaine();
	this.client = client;
    }

    public String traiteInvocation(String invocation){
    	String tab[] = invocation.split(":");
       	String resp = "RESP:";
       	String tmp[] = tab[2].split(",");
       	String param = tmp[1].substring(1, tmp[1].length()-2);
       	switch(tab[1]) {
       	case "nombreMots":
       		resp+="int:"+alc.nombreMots(param);
       		break;
       	case "asphyxie":
       		try {
				resp+="string:"+alc.asphyxie(param);
			} catch (PasDAirException e) {
				return "RESP:string:Déjà asphyxié\"";
			}
       		break;
       	case "leetSpeak":
    		resp+="string:"+alc.leetSpeak(param);
    		break;
       	case "compteChar":
       		String tmp2[] = tab[3].split(",");
       		String param2 = tmp2[1].substring(1, tmp2[1].length()-2);
       		resp+="int:"+alc.compteChar(param, param2.charAt(0));
       		break;
       	}
    	return resp;
    }
    
    public void traiteRequete() throws IOException {
    	BufferedReader reception = new BufferedReader(new InputStreamReader(client.getInputStream()));
		String requete = reception.readLine();
		String res = this.traiteInvocation(requete);
		PrintWriter envoi = new PrintWriter(client.getOutputStream(),true);
		envoi.println(res);
		client.close();
    }
}
